# findAppleDocs
### Usage:

`./findAppleDocs HT205001 /path/to/results.[csv || md]`

### Info:
The Article Number supplied will be pre-fixed with 'https://support.apple.com/en-us/'

### Output Format:
Ending the destination with `.csv or .md` will adjust the output accordingly.  If you don't specify a full path, the file will be placed in the current working directory.
